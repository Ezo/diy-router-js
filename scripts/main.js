/**
 * main.js
 * Ce fichier sert à démarrer l'application.
 */
var routes = [
    {
        path: 'Home', // http://localhost:8080/#Home
        view: 'home', // Vue = /views/home.html
        viewModel: HomeViewModel, // Voir /view-models/home-view-model.js
        isDefault: true // Si aucune route ne correspond, charger cette vue
    }, {
        path: 'About',
        view: 'about',
        viewModel: AboutViewModel
    }
];

/**
 * Module Application
 * Démarre l'application en initialisant le routeur
 */
var Application = (function (router, routes) {
    router.initializeRoutes(routes);

    /**
     * Exposition des membres publiques
     */
    return {

    };
})(ApplicationRouter, routes);
