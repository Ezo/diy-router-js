/**
 * Module AboutViewModel
 * Objet permettant de stocker l'état de la vue about.html
 * et de réagir au comportement de l'utilisateur.
 */
var AboutViewModel = (function () {
    var _onInit = function () {
        console.log('AboutViewModel::onInit()');
    };

    var _onDestroy = function () {
        console.log('AboutViewModel::onDestroy()');
    };

    return {
        onInit: _onInit,
        onDestroy: _onDestroy
    };
})();