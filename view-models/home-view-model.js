/**
 * Module HomeViewModel
 * Objet permettant de stocker l'état de la vue home.html
 * et de réagir au comportement de l'utilisateur.
 */
var HomeViewModel = (function () {
    var _onInit = function () {
        console.log('HomeViewModel::onInit()');
    };

    var _onDestroy = function () {
        console.log('HomeViewModel::onDestroy()');
    };

    return {
        onInit: _onInit,
        onDestroy: _onDestroy
    };
})();