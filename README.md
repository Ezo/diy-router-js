# DIY Routeur en JavaScript
Considérant la popularité des _framworks_ SPA (Single-Page Application ou application monopage),
il est tout à fait normal d'être curieux quant aux fonctionnalités de ces derniers.

Ce dépôt contient le code source du deuxième article de la série DIY (Do It Yourself) du blogue d'Ezo. L'article traite 
d'un routeur _frontend_ en JavaScript pour créer une SPA.

http://blog.ezoqc.com/diy-2-un-routeur-spa-en-javascript/

Merci de nous lire chaque semaine.

L'équipe d'Ezo

